# Introduction 
This is a demo of Tic Tac Toe game. It is created with React.js.

## React Version
^18.2.0

## Target Device
Desktop

## Gitlab CI Link
https://react-tic-tac-toe-junkit1-9fb7dfb03cb2acffc5ef19e093b325ca65a3d.gitlab.io