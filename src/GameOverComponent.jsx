import React from "react";

const GameOverComponent = (props) => {
    return <div className="winner">
        {props.isGameOver && (
            <p>`Player {props.player} has won!~~`</p>)
        }  
    </div>;
};

export default GameOverComponent;
