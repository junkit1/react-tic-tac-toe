import React from "react";

const SquareComponent = (props) => {
  const classes = props.className ? `${props.className} square` : `square`;
  const { onClick, state } = props;
  return (
    <div className={classes} onClick={onClick}>
      {state}
    </div>
  );
};

export default SquareComponent;
