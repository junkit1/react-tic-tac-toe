import React, { useState, useEffect } from "react";
import SquareComponent from "./SquareComponent";
import GameOverComponent from "./GameOverComponent";
import "./index.css";

let playerTurn = true;
const initiateState = ["", "", "", "", "", "", "", "", ""];
const playerChoice = "X";
const botChoice = "O";
const winningConditionLines = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6]
];

export default function App() {
  const [gameState, updateGameState] = useState(initiateState);
  const [isGameOver, updateGameOver] = useState(false);

  const onSquareClicked = (index) => {
    let boardStrings = Array.from(gameState);
    //to disallow player select an ocupied square
    if (boardStrings[index] !== "" || isGameOver) {
      return;
    }
    boardStrings[index] = playerTurn ? playerChoice : botChoice;
    updateGameState(boardStrings);
    playerTurn = !playerTurn;
    //updatePlayerTurn(!isPlayerTurn);
  };

  const initiateBotLogic = () => {
    let choosenIndex = null;
    //trying to search for condition to win or block player from winning
    for (let i = 0; i < winningConditionLines.length; i++) {
      const [a, b, c] = winningConditionLines[i];
      if (
        gameState[a] === "" &&
        gameState[b] !== "" &&
        gameState[b] === gameState[c]
      ) {
        choosenIndex = a;
        break;
      }
      if (
        gameState[b] === "" &&
        gameState[a] !== "" &&
        gameState[a] === gameState[c]
      ) {
        choosenIndex = b;
        break;
      }

      if (
        gameState[c] === "" &&
        gameState[b] !== "" &&
        gameState[b] === gameState[a]
      ) {
        choosenIndex = c;
        break;
      }
    }
    //if no choosenindex, random select
    if (choosenIndex === null) {
      let boardStrings = Array.from(gameState);
      const emptySpaces = [];
      for (let i = 0; i < boardStrings.length; i++) {
        if (boardStrings[i] === "") {
          emptySpaces.push(i);
        }
      }
      choosenIndex =
        emptySpaces[Math.floor(Math.random() * emptySpaces.length)];
    }
    onSquareClicked(choosenIndex);
  };

  const checkWinner = () => {
    //the winning lines of three matching owner
    for (let i = 0; i < winningConditionLines.length; i++) {
      const [a, b, c] = winningConditionLines[i];
      //if not equal to null and using a to check matching b and c
      if (
        gameState[a] &&
        gameState[a] === gameState[b] &&
        gameState[a] === gameState[c]
      ) {
        return gameState[a];
      }
    }
    return null;
  };

  useEffect(() => {
    //everytimes gameState changed, this will trigger
    let winner = checkWinner();
    if (winner) {
      updateGameOver(true);
    } else {
      if (!playerTurn) {
        initiateBotLogic();
      }
    }
  }, [gameState]);

  return (
    <div className="container">
      <div className="heading-text">Welcome to Tic Tac Toe</div>
      <div>
        <div className="row jc-center">
          <SquareComponent
            className="b-bottom-right"
            state={gameState[0]}
            onClick={() => onSquareClicked(0)}
          />
          <SquareComponent
            className="b-bottom-right"
            state={gameState[1]}
            onClick={() => onSquareClicked(1)}
          />
          <SquareComponent
            className="b-bottom"
            state={gameState[2]}
            onClick={() => onSquareClicked(2)}
          />
        </div>
        <div className="row jc-center">
          <SquareComponent
            className="b-bottom-right"
            state={gameState[3]}
            onClick={() => onSquareClicked(3)}
          />
          <SquareComponent
            className="b-bottom-right"
            state={gameState[4]}
            onClick={() => onSquareClicked(4)}
          />
          <SquareComponent
            className="b-bottom"
            state={gameState[5]}
            onClick={() => onSquareClicked(5)}
          />
        </div>
        <div className="row jc-center">
          <SquareComponent
            className="b-right"
            state={gameState[6]}
            onClick={() => onSquareClicked(6)}
          />
          <SquareComponent
            className="b-right"
            state={gameState[7]}
            onClick={() => onSquareClicked(7)}
          />
          <SquareComponent
            state={gameState[8]}
            onClick={() => onSquareClicked(8)}
          />
        </div>
        <GameOverComponent player={!playerTurn ? playerChoice : botChoice} isGameOver={isGameOver}/>
      </div>
      <button
        className="clear-button"
        onClick={() => {
          updateGameState(initiateState);
          playerTurn = true;
          updateGameOver(false);
        }}
      >
        Clear
      </button>
    </div>
  );
}
